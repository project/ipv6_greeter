<?php

/**
 * @file
 * Administration settings for ipv6_greeter
 */

/**
 * Administration settings form.
 *
 * @return
 *   The completed form definition.
 */
function ipv6_greeter_admin_settings() {
  $form = array();

  $form['ipv6_greeter_general_settings'] = array(
    '#type'  => 'fieldset',
    '#title' => t('General'),
  );
  $form['ipv6_greeter_general_settings']['ipv6_greeter_blocktitle'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Block title'),
    '#description'   => t('Title to show on top of the block, leave empty if you want no title'),
    '#default_value' => variable_get('ipv6_greeter_blocktitle', 'IPv6 Greeter'),
  );

  $form['ipv6_greeter_general_settings']['ipv6_greeter_greetIPv4'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Greet IPv4 too'),
    '#description'   => t('Greet also IPv4 clients, showing them info about IPv6.'),
    '#default_value' => variable_get('ipv6_greeter_greetIPv4', '1'),
  );

  return system_settings_form($form);
}
