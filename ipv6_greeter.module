<?php

/**
 * @file
 * Standalone module file to handle ipv6_greeter
 */

/**
 * Implementation of hook_perm().
 *
 * @return
 */
function ipv6_greeter_perm() {
  $perms[] = 'administer IPv6 Greeter';
  return $perms;
}

/**
 * Implementation of hook_block().
 *
 * @param object $op [optional]
 * @param object $delta [optional]
 * @return
 */
function ipv6_greeter_block($op = 'list', $delta = 0) {
  switch ($op) {

  case 'list':
    $blocks[0]['info'] = 'IPv6 Greeter';
    return $blocks;

  case 'view':
    $title = filter_xss_admin(variable_get('ipv6_greeter_blocktitle', 'IPv6 Greeter'));
    if (!empty($title)) {
      $block['subject'] = t($title);
    }

    $client_ip = ip_address();
    $is_client_ipv6 = filter_var($client_ip, FILTER_VALIDATE_IP, array("flags" => FILTER_FLAG_IPV6));
    $greet_ipv4 = variable_get('ipv6_greeter_greetIPv4', '1');

    $block['content'] = theme('ipv6_greeter', $client_ip, $is_client_ipv6, $greet_ipv4);
    $block['cache'] = BLOCK_NO_CACHE;

    return $block;
  
  }
}

/**
 * Implementation of hook_menu().
 */
function ipv6_greeter_menu() {
  $items = array();

  $items['admin/settings/ipv6_greeter'] = array(
    'title'            => 'IPv6 Greeter',
    'description'      => 'Setting for IPv6 Greeter',
    'page callback'    => 'drupal_get_form',
    'page arguments'   => array('ipv6_greeter_admin_settings'),
    'access arguments' => array('administer IPv6 Greeter'),
    'file'             => 'ipv6_greeter.admin.inc',
  );

  return $items;
}

/**
 * Implementation of hook_theme().
 */
function ipv6_greeter_theme() {
  return array(
    'ipv6_greeter' => array(
      'arguments' => array('client_ip' => NULL, 'is_client_ipv6' => NULL, 'greet_ipv4' => NULL),
    ),
  );
}

/**
 * Theme function for theming an IPv6 Greeter block.
 *
 * @param $client_ip
 *   The client IP address.
 * @param $is_client_ipv6
 *   TRUE if the client address is an IPv6 one, FALSE otherwise.
 * @param greet_ipv4
 *   Whether to greet the IPv4 clients or not.
 * @return
 *   An HTML themed string.
 */
function theme_ipv6_greeter($client_ip, $is_client_ipv6, $greet_ipv4) {
  drupal_add_css((drupal_get_path('module', 'ipv6_greeter') .'/ipv6_greeter.css'));

  $output = "";

  if ($is_client_ipv6) {
    $output = "<div class=\"ipv6_greeter-ipv6-client\"><p>";
    $output .= t("You've got IPv6!<br />!client_ip",
      array('!client_ip' => $client_ip));
    $output .= "</p></div>";
  }
  else {
    if ($greet_ipv4 == 1) {
      $ipv6 = l('IPv6', 'http://en.wikipedia.org/wiki/IPv6',
        array( 'attributes' => array('title' => 'Wikipedia - IPv6'))
      );
      $output = "<div class=\"ipv6_greeter-ipv4-client\"><p>";
      $output .= t("You're not on $ipv6, yet!");
      $output .= "</p></div>";
    }
  }

  return $output;
}
